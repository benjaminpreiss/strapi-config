# Strapi configuration and schema tracking

A repository to track strapi configuration and schema.
This repository assumes that there is a sibling directory "strapi" which is the local strapi docker app.

## Getting started

- Fork and clone the repository
- Make all scripts executable: `chmod u+x *.sh`
- Every time you edit the schemes in the strapi docker app, run `get-schema-from-local.sh` and commit the copied files to sync with production
